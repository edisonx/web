
	var InterValObj; //timer变量，控制时间
	var count = 60; //间隔函数，1秒执行
	var curCount;//当前剩余秒数
	var openid = window.localStorage.getItem('openid');
	var user_id = window.localStorage.getItem('user_id');
	var mstr = null;

    luckyDraw();

	// function sendMessage() {
	//   　curCount = count;
	// 　　//设置button效果，开始计时
	// 	 $("#btnSendCode").attr("disabled", "true");
	// 	 $("#btnSendCode").val("" + curCount + "秒后重新获取");
	// 	 InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次
	// 	 var dealType ="";
	// 	 var uid = "";
	// 	 var code = "";
	// 　　  //向后台发送处理数据
	// 	 $.ajax({
	// 	 　　type: "POST", //用POST方式传输
	// 	 　　dataType: "JSON", //数据格式:JSON
	// 	 　　url: '', //目标地址
	// 	　　 data: "dealType=" + dealType +"&uid=" + uid + "&code=" + code,
	// 	　　 error: function (data) { },
	// 	 　　success: function (msg){ }
	// 	 });
	// }

	//timer处理函数
	function SetRemainTime() {
		if (curCount == 0) {
			window.clearInterval(InterValObj);//停止计时器
			$("#btnSendCode").removeAttr("disabled");//启用按钮
			$("#btnSendCode").val("重新获取验证码").css({"background-color":"#0097a8"});
		}
		else {
			curCount--;
			$("#btnSendCode").val("" + curCount + "秒后重新获取").css({"background-color":"#D1D4D3"});
		}
	}

	function sendCode() {
		console.log("sending code");
		var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
		if(!myreg.test($("#add_phone").val())) {
			layertest('请输入有效的手机号码')
		    return false;
		}else{
			var url = 'http://h5.edisonx.cn/718_audio/api.php';
			$.ajax({
				url: url,
				type: "post",
				jsonType: "json",
				timeout: 10000,
				data: {a:"sendNodeCode",mobile:$("#add_phone").val()},
				success: function (data) {
					console.log('send code success');
					if (data['error'] == 0) {
						onSendCodeResult(true,data.data.mstr);
					} else {
                        if (data.info == "mobile already exist") {
                            data.info = "该手机号已注册过了";
                        } else if (data.info == "mobile error") {
                            data.info = "无效的手机号";
                        } else if (data.info == "note send fail") {
                            data.info = "短信发送失败";
                        }

						onSendCodeResult(false, data.info);
					}

				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					// console.log('error');
					onSendCodeResult(false, textStatus);
				},
				complete: function (XMLHttpRequest, status) {
					// console.log('complete: ');
					console.log(status);

					//请求完成后最终执行参数
					if (status == 'timeout') {
						onSendCodeResult(false, "请求超时");
					}
				}
			});
		}
	}

	var AddInterValObj; //timer变量，控制时间
	var adcount = 60; //间隔函数，1秒执行
	var addCount;//当前剩余秒数
	var loading_layer_index = 0;

	function onSendCodeResult (isSuccess, info) {
		console.log("onSendCodeResult");
        if (isSuccess) {
            addCount = adcount;
		　　//设置button效果，开始计时
			$("#addSendCode").attr("disabled", "true");
			$("#addSendCode").val("" + addCount + "秒后重新获取").css({"background-color":"#D1D4D3"});
			AddInterValObj = window.setInterval(SetAddnTime, 1000); //启动计时器，1秒执行一次
            mstr = info;
            // console.log("mstr=" + mstr);
        } else {
			layertest(info);
		}
    }

	//timer处理函数
	function SetAddnTime() {
		if (addCount == 0) {
			window.clearInterval(AddInterValObj);//停止计时器
			$("#addSendCode").removeAttr("disabled");//启用按钮
			$("#addSendCode").val("重新获取验证码").css({"background-color":"#0097a8"});
		}
		else {
			addCount--;
			$("#addSendCode").val("" + addCount + "秒后重新获取").css({"background-color":"#D1D4D3"});
		}
	}

	function telphone(){
		var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
		if(!myreg.test($("#add_phone").val()))
		{
			layertest('请输入有效的手机号码');
			$('.login_ipt').addClass('error');
		    return false;
		}else{
			$('.login_ipt').removeClass('error');
		}
	}
	$(document).on('blur','.login_ipt',function(){
		telphone();
	});

	//code 验证
	function code_test(){
		if($('#code').val()==''){
			layertest('验证码不能为空');
			$('#code').addClass('error');
		}else{
			$('#code').removeClass('error');
		}
	}
	$(document).on('blur','.code',function(){
		code_test();
	});

	// layer modal
	function layertest(content){
		layer.open({
		    content: content
		    ,btn: '我知道了'
		});
	}
	//layer loading
	function loading(content){
		return layer.open({
		    type: 2
		    ,content: content
		});
	}

	function close_loading(index){
    	layer.close(index);
	}

	function bindMobile() {
		console.log("binding mobile openid = " + openid);

		var url = 'http://h5.edisonx.cn/718_audio/api.php';
		$.ajax({
			url: url,
			type: "post",
			jsonType: "json",
			timeout: 10000,
			data: {a:"bindMobile",code:$("#code").val(),openid:openid, mstr:mstr},
			success: function (data) {
				// console.log('success');
				// var_dump(data);
				if (data['error'] == 0) {
					onBindMobileResult(true);
				} else {
					console.log(data.info);
					if (data.info == "code error") {
						data.info = "验证码错误";
					} else if (data.info == "openid error") {
                        data.info = "openid错误";
                    } else if (data.info == "mobile is binding") {
                        data.info = "您的手机已绑定,不需要再绑定了";
                    } else if (data.info == "note send fail") {
                        data.info = "短信发送失败";
                    }
					onBindMobileResult(false, data.info);
				}

			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log('error');
				onBindMobileResult(false, textStatus);
			},
			complete: function (XMLHttpRequest, status) {
				console.log('complete: ');
				console.log(status);

				//请求完成后最终执行参数
				if (status == 'timeout') {
					onBindMobileResult(false, "请求超时");
				}
			}
		});

	}

	function onBindMobileResult (isSuccess, info) {
        close_loading(loading_layer_index);
		if (isSuccess) {
			console.log("绑定成功");
			toLottery();
		} else {
			layertest(info);
		}
	}
	// update btn click
	$(document).on('click','.updateBtn',function(){
		if($('.error').length >0){
			layertest('请您填写正确的手机号和验证码');
		}else{
            loading_layer_index = loading('跳转中');
            bindMobile();
		}
	})

	function var_dump(obj){
        var des = "";
        for(var name in obj){
            des += name + ":" + obj[name] + ";";
        }
        console.log(des);
    }

    function luckyDraw() {
        var url = 'http://h5.edisonx.cn/718_audio/api.php';

        console.log("luckyDraw user_id=" + user_id);

        $.ajax({
            url: url,
            type: "post",
            jsonType: "json",
            timeout: 10000,
            data: {a:"luckyDraw"},
            success: function (data) {
                if (data['error'] == 0) {
                    var_dump(data.data);
					if (data.data['lucky'] == true) {
                    	window.localStorage.setItem("coupon_id", data.data.coupon_id);
                        console.log("reward = " + data.data.coupon_id);
					}
                } else {
                    console.log(data.info);
                }
				var_dump(data);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log('error');
            },
            complete: function (XMLHttpRequest, status) {
                console.log(status);
                if (status == 'timeout') {
                    console.log("time out");
                }
            }
        });
    }

	function toLottery () {
		window.location.href = "http://h5.edisonx.cn/718_audio/flip/lottery/index.html";
	}

