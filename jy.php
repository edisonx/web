<?php

$query_string = $_SERVER['QUERY_STRING'];
//echo "fetchImg ".$query_string;
$param_array = convertUrlQuery($query_string);
var_dump($param_array);

$access_token = $param_array['at'];
//echo 'access_token='.$access_token + "\n";
$media_id = $param_array['mid'] + "\n";
//echo 'media_id='.$media_id + "\n";
$img_url = 'https://qyapi.weixin.qq.com/cgi-bin/media/get?access_token='.$access_token.'&media_id='.$media_id;
download($img_url);

function download($url, $path = 'img/')
{
    echo "downloading $url";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    $file = curl_exec($ch);
    curl_close($ch);
    $filename = pathinfo($url, PATHINFO_BASENAME);
    $resource = fopen($path . $filename, 'a');
    fwrite($resource, $file);
    fclose($resource);
    echo 'done';
}

function convertUrlQuery($query)
{
    $queryParts = explode('&', $query);
    $params = array();
    foreach ($queryParts as $param) {
        $item = explode('=', $param);
        $params[$item[0]] = $item[1];
    }
    return $params;
}

?>