<?php

if (isset($_POST['upload'])) {
//    var_dump($_FILES);
    $result = array('error'=>$_FILES['upfile']['error']);

    if ($result['error'] === 0) {
        $app_name = $_POST['app_name'];
//        echo $app_name;
        if (isset($app_name)) {
            $app_version = $_POST['app_version'];
//            echo $app_version;
            if (isset($app_version)) {
                $release_notes = $_POST['release_notes'];
//                echo $release_notes;
                if (isset($release_notes)) {
                    $file_type = $_POST['file_type'];
                    $target_path = dirname(__FILE__) . "/app_pub/$app_name-$app_version." . $file_type;
//                    echo $target_path;
                    if (move_uploaded_file($_FILES['upfile']['tmp_name'], $target_path) != false) {
                        $zip_path = dirname($target_path) . "/$app_name-$app_version.zip";
                        if ($file_type !== 'zip') {
                            zipFile($target_path, $zip_path);
                        }
                        
                        $result['error'] = save2Db($app_name, $app_version, $release_notes, $zip_path, $file_type);
                    } else {
                        $result['upfile'] = $_FILES['upfile']['error'];
                        $result['tmp_name'] = $_FILES['upfile']['tmp_name'];
                        $result['error'] = 104;
                    }
                } else {
                    $result['error'] = 103;
                }
            } else {
                $result['error'] = 102;
            }
        } else {
            $result['error'] = 101;
        }
    }
    echo json_encode($result);
}

function zipFile($file_path, $zip_file_path) {
    $zip = new ZipArchive();
    $zip->open($zip_file_path,ZipArchive::CREATE);   //打开压缩包
    $zip->addFile($file_path,basename($file_path));   //向压缩包中添加文件
    $zip->close();  //关闭压缩包
}

function save2Db ($app_name, $app_version, $release_notes, $file_path, $file_type) {
    $error = 0;
    $db_connection = mysql_connect("localhost","root","e5cda60c7e");

    mysql_query("set names 'utf8'"); //数据库输出编码

    mysql_select_db("game"); //打开数据库

    $curtime = toDTS(curSystime());

    $sql = "select * from app_pub where app_name = '$app_name' and file_type = '$file_type' ORDER BY version DESC LIMIT 1";

//    echo $sql;

    $result = mysql_query($sql);

    if ($result !== false) { //已经有该app的版本了

        $msg = mysql_fetch_array($result);

        $cur_version = $msg['version'];

        if ($cur_version >= $app_version) { // 不比当前版本高
            $error = 105;
//            echo $error;
        }
    }

    if ($error === 0) {
        $os = "";
        if($file_type === 'exe') {
            $os = "win";
        } else if($file_type === 'zip'){
            $os = "linux";
        }

        $sql = "insert into app_pub (app_name,version,release_notes,upload_time,file_path,file_type,os) 
        values ('$app_name','$app_version','$release_notes','$curtime','$file_path','$file_type','$os')";
        mysql_query($sql);
    }

    mysql_close();
    return $error;
}

function curSystime() {
    list($t1, $t2) = explode(' ', microtime());
    return (float)sprintf('%.0f',(floatval($t1)+floatval($t2))*1000);
}

function toDTS($value) {
    if ($value === 0) {
        return '0';
    } else {
        return date("Y-m-d@H:i:s" , substr($value,0,10));
    }
}

?>